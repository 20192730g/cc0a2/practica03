package pe.uni.fiorellamezar.practica03;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.RadioButton;

import com.google.android.material.snackbar.Snackbar;

public class MainActivity extends AppCompatActivity {
    RadioButton radioButtonBasica, radioButtonCientifica, radioButtonProgramador;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        radioButtonBasica = findViewById(R.id.radio_button_basica);
        radioButtonCientifica = findViewById(R.id.radio_button_cientifica);
        radioButtonProgramador = findViewById(R.id.radio_button_programador);

        button = findViewById(R.id.button_start);

        button.setOnClickListener(v-> {
            if(!radioButtonBasica.isChecked() && !radioButtonCientifica.isChecked() && !radioButtonProgramador.isChecked()){
                Snackbar.make(v, R.string.snack_bar_msg, Snackbar.LENGTH_LONG).show();
                return;
            }

            Intent intent = new Intent(MainActivity.this, MiCalculadoraActivity.class);

            if(radioButtonBasica.isChecked()){
                intent.putExtra("BASICA", true);
                startActivity(intent);
            }

            if(radioButtonCientifica.isChecked()){
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
                alertDialog
                        .setTitle(R.string.dialog_title)
                        .setMessage(R.string.dialog_msg)
                        .setNegativeButton(R.string.no, (dialog, which) -> {
                            moveTaskToBack(true);
                            android.os.Process.killProcess(android.os.Process.myPid());
                            System.exit(1);
                        })
                        .setPositiveButton(R.string.yes, (dialog, which) -> {
                            //
                        }).show();
                alertDialog.create();
            }

            if(radioButtonProgramador.isChecked()){
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
                alertDialog
                        .setTitle(R.string.dialog_title)
                        .setMessage(R.string.dialog_msg)
                        .setNegativeButton(R.string.no, (dialog, which) -> {
                            moveTaskToBack(true);
                            android.os.Process.killProcess(android.os.Process.myPid());
                            System.exit(1);
                        })
                        .setPositiveButton(R.string.yes, (dialog, which) -> {
                            //
                        }).show();
                alertDialog.create();

            }
        });
    }
}