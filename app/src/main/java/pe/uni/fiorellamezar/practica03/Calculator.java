package pe.uni.fiorellamezar.practica03;

public class Calculator {
    public int sumar(int a, int b){
        return a+b;
    }
    public int restar(int a, int b){
        return a-b;
    }
    public int multiplicar(int a, int b){
        return a*b;
    }
    public int dividir(int a, int b){
        return a/b;
    }
}
