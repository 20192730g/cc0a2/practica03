package pe.uni.fiorellamezar.practica03;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

@SuppressLint("CustomSplashScreen")
public class SplashActivity extends AppCompatActivity {
    TextView textViewSlash;
    ImageView imageViewSlash;

    Animation animationImage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        imageViewSlash = findViewById(R.id.image_view_splash);
        textViewSlash = findViewById(R.id.text_view_splash);

        animationImage = AnimationUtils.loadAnimation(this, R.anim.image_animation);

        imageViewSlash.setAnimation(animationImage);

        new CountDownTimer(6000, 1000){
            @Override
            public void onTick(long millisUntilFinished){

            }

            @Override
            public void onFinish() {
                Intent intent= new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }.start();
    }
}