package pe.uni.fiorellamezar.practica03;

import androidx.appcompat.app.AppCompatActivity;

import android.content.res.Resources;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

public class MiCalculadoraActivity extends AppCompatActivity {
    Button button1, button2, button3, button4, button5, button6, button7, button8, button9, button0;
    Button buttonSuma, buttonResta, buttonDividir, buttonMultiplicar;
    Button buttonBorrar, buttonResultado;
    TextView textViewCaluladora;
    Calculator calculadora = new Calculator();
    int a,b,resultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mi_calculadora);

        button1 = findViewById(R.id.button_1);
        button2 = findViewById(R.id.button_2);
        button3 = findViewById(R.id.button_3);
        button4 = findViewById(R.id.button_4);
        button5 = findViewById(R.id.button_5);
        button6 = findViewById(R.id.button_6);
        button7 = findViewById(R.id.button_7);
        button8 = findViewById(R.id.button_8);
        button9 = findViewById(R.id.button_9);
        button0 = findViewById(R.id.button_0);

        buttonSuma  = findViewById(R.id.button_sumar);
        buttonResta = findViewById(R.id.button_restar);
        buttonDividir = findViewById(R.id.button_dividir);
        buttonMultiplicar = findViewById(R.id.button_multiplicar);
        buttonBorrar = findViewById(R.id.button_borrar);
        buttonResultado = findViewById(R.id.button_resultado);

        Resources res = getResources();
        button1.setOnClickListener(v -> textViewCaluladora.setText(R.string.button_1));
        button0.setOnClickListener(v -> textViewCaluladora.setText(R.string.button_0));
        


    }
}